
import  java.sql.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
public class connect_db {
    public static void main(String[] arg)
    {

        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Stock_Mangement_Db", "postgres", "Mony1144");
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM stock_management");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String unit_price = rs.getString("unit_price");
                String qty = rs.getString("qty");
                String import_date = rs.getString("import_date");

                System.out.println("Id =" + id);
                System.out.println("Name=" + name);
                System.out.println("unit_price=" + unit_price);
                System.out.println("qty=" + qty);
                System.out.println("import_date=" + import_date);
            }
            rs.close();
            stmt.close();
            c.close();
        }
        catch(Exception e)
        {
            System.out.println(e.getClass().getName()+":"+e.getMessage());
            System.exit(0);
        }
    }
    }

